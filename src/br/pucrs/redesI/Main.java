package br.pucrs.redesI;

import java.io.IOException;

import br.pucrs.redesI.packages.UDPMachine;
import br.pucrs.redesI.utils.Utils;


/**
 * Classe apenas utilizada para iniciar um servidor 
 */
public class Main {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Utils.CHUNK_SIZE = args.length > 0 ? new Integer(args[0]) : 30000;
		Utils.MAX_TIME = args.length > 1 ? new Integer(args[1]) * 1000 : 5000;
		Utils.MIN_TIME = args.length > 2 ? new Integer(args[2]) * 1000 : 2000;
		Utils.MAGIC_NUMBER = args.length > 3 ? new Integer(args[3]) : 20;

		UDPMachine udpServer1 = new UDPMachine();
		udpServer1.start();	
	}

}
