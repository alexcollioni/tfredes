package br.pucrs.redesI.packages;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import br.pucrs.redesI.utils.Address;
import br.pucrs.redesI.utils.Utils;

/**
 * Representação de um datagrama
 */
public class Token implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum Response {
		ACK, NACK
	}

	// HEAD
	private Date time;
	private int fileLenght;
	private int currentPart;
	private int totalParts;
	private Address destination;
	private Address origin;
	private Response response;
	private String CRC;

	// DATA
	byte[] data;

	public Token() {
		this.time = new Date();
		this.currentPart = 0;
		this.totalParts = 0;
		this.destination = null;
		this.data = null;
	}

	public Token(int currentPart, int totalParts, Address destination, byte[] data, int currPort) throws IOException {
		this();
		this.currentPart = currentPart;
		this.totalParts = totalParts;
		this.destination = destination;
		this.data = data;
		this.origin = Utils.getCurrentAddress(currPort);
	}

	public Date getTime() {
		return time;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String toString() {
		String hour = this.time.getHours() + ":" + this.time.getMinutes() + ":" + this.time.getSeconds();
		String d = this.data == null ? "" : " DATA [" + this.data[0] + " " + this.data[1] + " " + this.data[2] + "...]";
		String ack = this.response == Response.ACK ? "<<ACK>>" : "";
		return ack + " " + hour + " dest " + this.destination + " orig " + this.origin + " - " + this.currentPart + "/"
				+ this.totalParts + " CRC " + CRC + d;
	}

	public void resetTime() {
		this.time = new Date();
	}

	public int getCurrentPart() {
		return currentPart;
	}

	public int getTotalParts() {
		return totalParts;
	}

	public Address getDestination() {
		return destination;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public void setCurrentPart(int currentPart) {
		this.currentPart = currentPart;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	public void setTotalParts(int totalParts) {
		this.totalParts = totalParts;
	}

	public Address getOrigin() {
		return origin;
	}

	public void setOrigin(Address origin) {
		this.origin = origin;
	}

	public byte[] getData() {
		return data;
	}

	public boolean isAck() {
		return response == Response.ACK;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setFileLenght(int fileLenght) {
		this.fileLenght = fileLenght;
	}

	public int getFileLenght() {
		return fileLenght;
	}

	public boolean isLastPackage() {
		return this.currentPart == this.totalParts;
	}

	public void setCRC(String cRC) {
		CRC = cRC;
	}

	public String getCRC() {
		return CRC;
	}

	public boolean isNack() {
		return this.response == Response.NACK;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
}
