package br.pucrs.redesI.packages;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import br.pucrs.redesI.packages.Token.Response;
import br.pucrs.redesI.utils.Address;
import br.pucrs.redesI.utils.CRC;
import br.pucrs.redesI.utils.Utils;

/**
 * * Representa��o de uma m�quina na rede * Lida com o recebimento e envio de
 * pacotes pela rede * Quando isMaster, controla o tempo que um pacote leva para
 * circular a rede
 * */
public class UDPMachine {
	private Address nextMachine;
	private Address currentAddress;
	private boolean startSendingFile;
	private boolean isMaster;
	private DatagramSocket serverSocket;
	private boolean killServer = false;
	private Address destAddress;
	private HashMap<String, byte[]> receivedFile = new HashMap<>();
	private int lastConfirmed = 0;
	private String fileNameToSend;
	private byte[] receiveData;

	/**
	 * Inicia uma thread para lidar do recebimento de pacotes Pede que o usu�rio
	 * informe o pr�ximo nodo da rede Caso queira, o usu�rio informa um arquivo
	 * para enviar a um nodo
	 * */
	public void start() throws IOException, ClassNotFoundException {
		int port = Utils.getFreePort();
		this.receiveData = new byte[30560];
		this.currentAddress = Utils.getCurrentAddress(port);
		this.serverSocket = new DatagramSocket(port);

		String msg = "Informe o pr�ximo nodo da rede:" + this.currentAddress;
		this.nextMachine = Utils.getAddress(Utils.readFromKeyboard(msg));

		this.isMaster = Utils.readFromKeyboard("Esta��o controla o token? 1 = sim, 2 = n�o").equals("1");
		System.out.println("Controla o token? " + isMaster);

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					runServer();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			private void runServer() throws IOException {
				// A master garante que caso um nodo seja perdido, outro ir� ser
				// inserido na rede
				if (isMaster) {
					serverSocket.setSoTimeout((int) Utils.MAX_TIME);
				}
				DatagramPacket receivePacket = null;
				boolean sucessfullyReceived = false;
				while (!killServer) {
					receivePacket = new DatagramPacket(receiveData, receiveData.length);
					sucessfullyReceived = false;
					try {
						serverSocket.receive(receivePacket);
						sucessfullyReceived = true;
					} catch (SocketTimeoutException e) {
						// O pacote n�o foi recebido dentro do tempo esperado
						System.out.println("TIMEOUT - Enviando novo token pela rede");
					} finally {
						if (sucessfullyReceived) {
							dealWithToken((Token) Utils.toObject(receivePacket.getData()));
						} else {
							sendData(new Token());
						}
					}
					receiveData = new byte[30560];
				}
			}
		}).start();
		if (this.isMaster) {
			sendData(new Token());
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				String msg = "Enviar arquivo para endereco (" + currentAddress + "):";
				String msgArquivo = "Nome do arquivo (" + currentAddress + "):";
				try {
					destAddress = Utils.getAddress(Utils.readFromKeyboard(msg));
					fileNameToSend = Utils.readFromKeyboard(msgArquivo);
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				startSendingFile = destAddress != null;
				if (startSendingFile)
					System.out.println("Destino " + destAddress);
			}
		}).start();
	}

	public void sendData(Token token) {
		// Perde um token pela rede
		if (new Random().nextInt(Utils.MAGIC_NUMBER) == 1) {
			System.out.println(">>> Gerando Timeout - Perdendo pacote " + token);
			return;
		}
		// Gera um 'ruido', invalidando o CRC
		if (token.getData() != null && new Random().nextInt(Utils.MAGIC_NUMBER) == 1) {
			System.out.println("Gerando NACK");
			byte aux = token.getData()[0];
			token.getData()[0] = token.getData()[1];
			token.getData()[1] = aux;
		}
		// A m�quina � o destino
		if (Utils.amITheDestination(token, this.currentAddress)) {
			// o pacote � um nack
			if (token.isNack()) {
				System.out.println(">>> NACK: " + token + " recebido");
				Date time = token.getTime();
				token = new Token();
				token.setTime(time);
				// o pacote � um ack
			} else if (token.isAck()) {
				if (token.isLastPackage()) {
					this.startSendingFile = false;
				}
				System.out.println("ACK: " + token + " entregue!");
				Date time = token.getTime();
				this.lastConfirmed = token.getCurrentPart();
				token = new Token();
				token.setTime(time);
				// o pacote contem dados
			} else if (token.getCurrentPart() > 0) {
				System.out.println("Recebendo pacote " + token);
				getDataFromToken(token);
			}
			// rede livre para enviar um pacote
		} else if (token.getCurrentPart() < 1 && this.startSendingFile) {
			token.setCurrentPart(this.lastConfirmed);
			prepareTokenToSend(token, fileNameToSend);
			token.setDestination(this.destAddress);
			System.out.println("Enviando pacote " + token);
		}

		byte[] sendData = new byte[Utils.CHUNK_SIZE];
		sendData = Utils.toBytes(token);
		Utils.wait(2);
		try {
			DatagramSocket clientSocket = new DatagramSocket();
			clientSocket.send(new DatagramPacket(sendData, sendData.length, nextMachine.getAddress(), nextMachine
					.getPort()));
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("- - - - - - - - - - - - - - - - - - -");
	}

	private void getDataFromToken(Token token) {
		if (token.getCurrentPart() == 1) {
			this.receivedFile.put(token.getDestination().getIp(), new byte[token.getFileLenght()]);
		}
		int tokenPtr = 0;
		byte[] data = token.getData();
		StringBuilder bitMsg = new StringBuilder();
		for (int x = 0; x < data.length; x++) {
			bitMsg.append(CRC.byteToBin(data[x]));
		}
		bitMsg.append(token.getCRC());
		if (new Integer(CRC.verifyCRC(bitMsg.toString())) != 0) {
			System.out.println("NAO PASSOU NO CRC - " + token);
			token.setResponse(Response.NACK);
		} else {
			System.out.println("PASSOU NO CRC" + token);
			token.setResponse(Response.ACK);

			int j = (token.isLastPackage() ? token.getFileLenght() % Utils.CHUNK_SIZE : Utils.CHUNK_SIZE);
			int i = (token.getCurrentPart() - 1) * Utils.CHUNK_SIZE;
			for (; tokenPtr < j; i++, tokenPtr++) {
				this.receivedFile.get(token.getDestination().getIp())[i] = data[tokenPtr];
			}
			if (token.getTotalParts() == token.getCurrentPart()) {
				System.out.println(">>> Aquivo recebido! - Transmiss�o encerrada <<<");
				String fileName = token.getOrigin().toString().replace(':', ' ') + " "
						+ token.getDestination().toString().replace(':', ' ') + ".png";
				Utils.writeFile(this.receivedFile.get(token.getDestination().getIp()), fileName);
			}
		}
		token.data = null;
		token.setDestination(token.getOrigin());
		token.setOrigin(this.currentAddress);
	}

	private void prepareTokenToSend(Token token, String fileOriginal) {
		byte[] originalFile = Utils.readFile(fileOriginal);
		int totalParts = originalFile.length / Utils.CHUNK_SIZE;
		totalParts += originalFile.length - (totalParts * Utils.CHUNK_SIZE) > 0 ? 1 : 0;
		token.setTotalParts(totalParts);
		token.setCurrentPart(token.getCurrentPart() + 1);
		token.setFileLenght(originalFile.length);

		int end = token.getCurrentPart() * Utils.CHUNK_SIZE > originalFile.length ? originalFile.length : token
				.getCurrentPart() * Utils.CHUNK_SIZE;
		int startsAt = (token.getCurrentPart() - 1) * Utils.CHUNK_SIZE;
		byte[] file = Utils.getSlice(originalFile, startsAt, end);
		token.setData(file);

		StringBuilder bitMsg = new StringBuilder();
		for (int x = 0; x < file.length; x++) {
			bitMsg.append(CRC.byteToBin(file[x]));
		}
		token.setCRC(CRC.calcCRC(bitMsg.toString()));

		token.setDestination(this.destAddress);
		token.setOrigin(this.currentAddress);
	}

	public void killServer() {
		this.killServer = true;
	}

	private void dealWithToken(Token t) {
		if (isMaster) {
			// E-A < max && E-A > min
			long a = new Date().getTime() - t.getTime().getTime();
			System.out.println("a => " + a);
			if (a > Utils.MAX_TIME) {
				System.out.println(" >>>> Token recebido antes do tempo esperado <<<<");
				System.out.println(" Token descartado = " + t);
			} else if (a < Utils.MIN_TIME) {
				System.out.println(" >>>> Token recebido antes do tempo esperado <<<<");
				System.out.println(" Token descartado = " + t);
			} else {
				System.out.println("Tempo de um ciclo " + (new Date().getTime() - t.getTime().getTime()));
				t.resetTime();
				sendData(t);
			}
		} else {
			sendData(t);
		}
	}
}
