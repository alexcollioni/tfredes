package br.pucrs.redesI.utils;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Representação de um endereço e porta
 * */
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	private InetAddress address;
	private int port;

	public Address() {
	}

	public Address(String address, int port) throws UnknownHostException {
		this.address = InetAddress.getByName(address);
		this.port = port;
	}

	@Override
	public String toString() {
		return this.address.getCanonicalHostName() + ":" + this.port;
	}

	public int getPort() {
		return port;
	}

	public InetAddress getAddress() {
		return address;
	}

	public String getIp() {
		return this.address.getHostAddress() + ":" + this.port;
	}
}
