package br.pucrs.redesI.utils;

/**
 * Classe respons�vel pela verifica��o de CRC
 */
public class CRC {

	private static final String POLINOMIO = "101001011";
	private static final int GRAU = 8;

	/**
	 * M�todo que retorna o CRC a partir de um chunk  
 	*/
	public static String calcCRC(String msg) {
		StringBuilder resto = new StringBuilder();
		StringBuilder str_in = new StringBuilder(msg);
		for (int x = 0; x < GRAU; x++) {
			str_in.append("0");
		}
		StringBuilder div = new StringBuilder(str_in.substring(0, 9));
		for (int x = 0; x < str_in.length() - 8; x++) {
			StringBuilder result = new StringBuilder();
			for (int y = 0; y < div.length(); y++) {
				if (div.charAt(0) == '1') {
					result.append(div.charAt(y) ^ POLINOMIO.charAt(y));
				} else {
					result.append(div.charAt(y) ^ '0');
				}
			}
			if (x != str_in.length() - 9) {
				div = new StringBuilder(result.substring(1) + str_in.charAt(x + 9));
			} else {
				resto = new StringBuilder(result.substring(1));
			}
		}
		return resto.toString();
	}

	/**
	 * Verifica se a mensagem � equivalente ao CRC
	 * Retorna 0 se CRC passou
	 * 	   
 	*/
	public static String verifyCRC(String msg) {
		StringBuilder resto = new StringBuilder();
		StringBuilder str_in = new StringBuilder(msg);
		StringBuilder div = new StringBuilder(str_in.substring(0, 9));
		for (int x = 0; x < str_in.length() - 8; x++) {
			StringBuilder result = new StringBuilder();
			for (int y = 0; y < div.length(); y++) {
				if (div.charAt(0) == '1') {
					result.append(div.charAt(y) ^ POLINOMIO.charAt(y));
				} else {
					result.append(div.charAt(y) ^ '0');
				}
			}
			if (x != str_in.length() - 9) {
				div = new StringBuilder(result.substring(1) + str_in.charAt(x + 9));
			} else {
				resto = new StringBuilder(result.substring(1));
			}
		}
		return resto.toString();
	}
	
	/**
	 *	Convers�o de string para byte
 	*/
	public static byte binToByte(String val) {
		long bin = Long.valueOf(val);
		long dec = 0;
		long pow = 1;
		long digit = bin % 10;
		while (bin != 0) {
			dec = dec + (pow * digit);
			bin = bin / 10;
			digit = bin % 10;
			pow = pow * 2;
		}
		return (byte) dec;
	}

	public static String byteToBin(byte dec) {
		return Integer.toBinaryString(dec);
	}
}