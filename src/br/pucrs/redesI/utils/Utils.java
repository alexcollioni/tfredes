package br.pucrs.redesI.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import br.pucrs.redesI.packages.Token;

/**
 * Classe que mant�m m�todos �teis a v�rias classes
 * Tamb�m mant�m contantes
 * */
public class Utils {
	private static final int FIRST_PORT = 9876;
	public static int CHUNK_SIZE = 3000;
	public static long MAX_TIME = 5 * 1000;
	public static long MIN_TIME = 2 * 1000;
	public static int MAGIC_NUMBER = 20;

	/**
	 * Testa se uma porta est� em uso
	 * */
	public static boolean isTaken(int port) {
		boolean portTaken = false;
		DatagramSocket datagramSocket = null;
		try {
			datagramSocket = new DatagramSocket(port);
		} catch (SocketException e) {
			portTaken = true;
		} finally {
			if (datagramSocket != null) {
				datagramSocket.close();
			}
		}
		return portTaken;
	}

/**
 *Retorna uma porta que n�o esteja em uso 
 * */
	public static int getFreePort() {
		int aux = FIRST_PORT;
		while (true) {
			System.out.print("A porta " + aux + " est� livre? ");
			if (!Utils.isTaken(aux)) {
				System.out.println(" Sim ");
				return aux;
			}
			System.out.println(" N�o ");
			aux++;
		}
	}

	public static void wait(int sec) {
		uWait(sec * 1000);
	}

	public static String readFromKeyboard(String msg) {
		return (String) JOptionPane.showInputDialog(msg);
	}

	public static int getPort(String param) {
		String[] split = param.split(":");
		return Integer.valueOf(split.length == 1 ? split[0] : split[1]);
	}

	public static String getIP(String param) {
		return param.split(":")[0];
	}

	public static Address getAddress(String param) throws UnknownHostException {
		if (param == null || param.isEmpty())
			return null;
		return new Address(Utils.getIP(param), Utils.getPort(param));
	}

	public static byte[] toBytes(Object object) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	public static Object toObject(byte[] bytes) {
		try {
			return new ObjectInputStream(new ByteArrayInputStream(bytes)).readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] readFile(String fileName) {
		String path = System.getProperty("user.dir") + System.getProperty("file.separator") + fileName;
		File file = new File(path);
		byte[] fileToBytes = new byte[(int) file.length()];
		FileInputStream fin;
		try {
			fin = new FileInputStream(file);
			fin.read(fileToBytes);
			fin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileToBytes;
	}

	public static void writeFile(byte[] fileToBytes, String fileName) {
		String path = System.getProperty("user.dir") + System.getProperty("file.separator") + fileName;
		File file = new File(path);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(fileToBytes);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Address getCurrentAddress(int port) {
		try {
			return new Address(InetAddress.getLocalHost().getHostAddress(), port);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean amITheOrigin(Token token, Address currentAddress) throws UnknownHostException {
		Address origin = token.getOrigin();
		if (origin == null)
			return false;
		return origin.getAddress() == currentAddress.getAddress() && origin.getPort() == currentAddress.getPort();
	}

	public static boolean amITheDestination(Token token, Address currentAddress) {
		Address destin = token.getDestination();
		if (destin == null)
			return false;
		return destin.getAddress().equals(currentAddress.getAddress()) && destin.getPort() == currentAddress.getPort();
	}

	/**
	 * Obtem uma 'fatia' do arquivo 
	 * */
	public static byte[] getSlice(byte[] file, int startsAt, int max) {
		byte[] slice = new byte[Utils.CHUNK_SIZE];
		int i = 0;
		for (; startsAt < max; startsAt++, i++) {
			slice[i] = file[startsAt];
		}
		return slice;
	}

	public static void uWait(int sec) {
		long t0, t1, time;
		time = sec;
		t0 = System.currentTimeMillis();
		do {
			t1 = System.currentTimeMillis();
		} while (t1 - t0 < time);
	}
}
